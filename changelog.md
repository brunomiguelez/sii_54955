# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2021-10-25
### Changed
-Now the file Mundo.cpp is signed with the name of the writer.

## [1.0.1] - 2021-10-26
### Added
-Added file readme.txt on branch practica2.

## [1.0.2] - 2021-11-11
### Added
-Added feature to tenis, now ball's size decreases with time.
